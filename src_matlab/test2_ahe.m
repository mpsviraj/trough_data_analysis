aa = dir('./final_data/sam*.mat')
%%
load('./final_data/calib_data.mat')


for i = 1: length(aa)
   file_name = strcat('./final_data/',aa(i).name);
  
   
   if (exist('data')==1)
       fprintf(2,'file unloaded\n')
       fprintf("folder :%s\n",aa(i).name);
       
   end
   load(file_name)
   tmp_data = data.data;
   
   
   fprintf("length :%d\n",length(tmp_data));
   t = tmp_data(:,1);
   spa = tmp_data(:,2);
   tr_angle = tmp_data(:,3);
   
   err = spa - tr_angle;
   filtr = abs(err)<1;
%    fprintf("good data:%d\n",sum(filtr))
%    
%    fprintf("max_temp :%4.2f\n",max(max(tmp_data(:,8:end))))
   dt = strsplit(aa(i).name,'_');
   dt = strsplit(dt{3},".");
   ttl = strcat(dt{1},', ',num2str(data.height),' cm')
   
   crrct_data = calib_temp(tmp_data(:,8:end),coeff); 
   [m,y]=max(crrct_data');
   fil_indx = abs(y-10)<5;
   subplot(3,3,i)
   imagesc(crrct_data(find(fil_indx),:))
   title(ttl)
   ylabel('#event')
   xlabel('sensor')
   
%    imagesc(crrct_data)
   
  colorbar()
   
%    ginput(1)    
end
