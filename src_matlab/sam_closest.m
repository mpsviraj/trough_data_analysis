function [ data_mod ] = sam_closest( data )
%SAM_CLOSEST Summary of this function goes here
%   Detailed explanation goes here
filtr = data(:,2)==4;
index2 = find(filtr);

data_mod = zeros(length(index2),27);

for t = 1:length(index2)
  i = index2(t);
  
  data_mod(t,1)=data(i,1);      %populate time
  
  clossest_ref = get_closest_data(data,i,9); %spa
  data_mod(t,2)=data(clossest_ref,3);
  
  clossest_ref = get_closest_data(data,i,8); %Trough angle
  data_mod(t,3)=data(clossest_ref,3);    %correction for shift +.8
  
  clossest_ref = get_closest_data(data,i,6); %Humidity
  data_mod(t,4)=data(clossest_ref,3);
    
  clossest_ref = get_closest_data(data,i,7); %room_temp
  data_mod(t,5)= data(clossest_ref,3);
  
  clossest_ref = get_closest_data(data,i,4); %IRR_2
  data_mod(t,6)= data(clossest_ref,3);
  
  clossest_ref = get_closest_data(data,i,5); %IRR_16
  data_mod(t,7)= data(clossest_ref,3);
  
  clossest_ref = get_closest_data(data,i,1); %IRR_16
  data_mod(t,8:27)= data(clossest_ref,3:22);

%%  printf(">")
%%  printf("%f,%f %f,%f\n",data(i,1),data(i,3),data(clossest_ref ,1),data(clossest_ref ,3))
end

end

