function [indx,val] = get_closest_data(data,indexref,type)
  
  time_ref = data(indexref,1);
  val_index = find(data(:,2)==type);
  dt = abs(data(val_index,1)-time_ref);
  [val indx] = min(dt);
  indx = val_index(indx);
%   fprintf("%f\n",val)

  

