

% load data manually
aa = dir('./final_data/*.mat');
file_name =strcat('./final_data/',aa(1).name)
load(file_name)

temp_data = data.data;
t_fixed = datetime(temp_data(:,1),'ConvertFrom','posixtime');
spa = temp_data(:,3)+.8;
tr_angle = temp_data(:,2);



plot(t_fixed,spa)
hold on
plot(t_fixed,tr_angle)

fil = abs(spa-tr_angle)<.5;

plot(t_fixed(fil),tr_angle(fil),'o','Linewidth',3)
hold on

% t_val = max(temp_data(:,8:end)').*40./max(max(temp_data(:,8:end)));
t_val = temp_data(:,18);
plot(t_fixed,t_val.*40./max(t_val))
% t_val = temp_data(:,8);
% plot(t_fixed,t_val.*40./max(t_val))
% 
% t_val = temp_data(:,end);
% plot(t_fixed,t_val.*40./max(t_val))




