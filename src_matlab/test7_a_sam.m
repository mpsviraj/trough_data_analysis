close
aa = dir('./final_data/sam*.mat');
    colorspec = {[0 0 0]; [0 1 0]; [1 0 0]; [0 0 1]; [0.6 0.1 0.5]; [0.8 0.4 0]; [0.4 0.7 0.2]};
    s_line = [2 3 1 1 1 1 2]; %manuelly selected best data set from previously select 5 points
h=[]
fwhm =[]
    for k=1:length(aa);
    file_name = strcat('./final_data/',aa(k).name);
    load(file_name)
    temp = data.calibrated();
     org_irr = data.data(:,6);
     err = abs(data.data(:,2)-data.data(:,3));
%      point = data.profile(s_line(k));
%     tr_ang = data.data(point,3);
        xx = 1:20;
        xx(16)=[];    
        j = 1
        lbl{k} = strcat(num2str(data.height),' cm');
for i = data.profile
    figure(1)
        
        plot(temp(i,:),'color',colorspec{k})  
        hold on  
        figure(3)
        ff = fittype('gauss1');
        f = fit(xx',temp(i,:)',ff)
        plot(xx,f(xx),'color',colorspec{k})
        hold on
        fwhm(k,j)= f.c1;
        fwhm(k,6) = data.height;
        j = j+1;
    end
%     plot(f)
    
    end 

title('Temp ptofiles of differnt levels')
xlabel('sensor')
ylabel('Temperature')
legend(h,lbl) 

figure(2)
plot(fwhm(:,6),fwhm(:,1:5),'o')
 