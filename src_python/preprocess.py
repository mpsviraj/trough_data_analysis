from pvlib import solarposition, tracking
import pandas as pd
import matplotlib.pyplot as plt
import math
import time
import os, fnmatch
import shutil
from tqdm import tqdm 

#Know referance
ref_180 = -55.55 + 4.18

referenceTabs ={
        'system':0,
        'C7BF713C':1,
        'acc':2,
        'gyro':3,
        'irrediance_02':4,
        'irrediance_16':5,
        'humi':6,
        'temp':7,
        'SPA':8,
        'trough_angle':9,
        'tempcal':1010,
        }


path = os.getcwd()
RepoPath = '../../trought_data' #"F:/DrivePHYS/UOC/Research/MPhil/MPhil_Part2_Trough/trough_data"
#files = os.listdir(RepoPath)

def wrap_angle(raw_angle):
    '''Wriapping angle to v2.1 Ref -55+4.18'''

    if raw_angle > ref_180:
        return raw_angle - (90 + ref_180)
    
    if raw_angle < ref_180:
        return raw_angle +90 - ref_180

def calc_spa(ts):
    '''Get SPA angle using time'''

    tz = 'Etc/UCT'
    lat, lon = 6.902103,79.860599
    
    time_stamp = pd.Timestamp(ts, unit='s')
    solpos = solarposition.get_solarposition(time_stamp, lat, lon)

    truetracking_angles = tracking.singleaxis(
        apparent_zenith=solpos['apparent_zenith'],
        apparent_azimuth=solpos['azimuth'],
        axis_tilt=0,
        axis_azimuth=0,
        max_angle=90,
        backtrack=False,  # for true-tracking
        gcr=0.5)  # irrelevant for true-tracking
    
    return truetracking_angles['tracker_theta'][0]

def read_file (file_Name):
    '''Reading and preprocessing log file'''
    ff =file_Name.split('/');
    lines = len(open(file_Name).readlines()) 
    bar = tqdm(range(lines)) 
    with open('../Data_Fixed_files/samfix-{}'.format(ff[-1]),'a+') as fixedFile:
    #with open(filePath+'/FixCSV_'+directory+'.csv','a') as fixedFile:   
        with open(file_Name,"r") as dataFile:
            for dataLine in dataFile:
                mqtt_data = dataLine.split(";")
                mqtt_dataFilt = mqtt_data[0].split(":")
                try:
                    aq_time = float(mqtt_data[0]);
                except:
                    continue
                topic = mqtt_data[1]
                data = mqtt_data[2];
                tags = ['system','C7BF713C','acc','gyro','irrediance_02','irrediance_16','humi','temp','SPA','trough_angle','tempcal'];
                dataTag = mqtt_data[1].split("/")
                if tags[-1] == dataTag[-1]:
                    line = "{},{},{}".format(aq_time,referenceTabs[tags[-1]],data)
                    if(line[-1]!='\n'):
                        line = '{}\n'.format(line)
                    fixedFile.write(line) 
                else:
                    for tag in tags:
                        if "status"in topic or "ACK" in data or "motor" in topic or "cmd" in topic:
                            break
                        if tag in mqtt_data[1]:
                            if (data == 'restart\n'):
                                data = -1111
                            line = "{},{},{}".format(aq_time,referenceTabs[tag],data)
                            if(line[-1]!='\n'):
                                line = '{}\n'.format(line)
                            fixedFile.write(line)
                            if tag == 'acc':
                                line ="{},{},{}".format(aq_time,referenceTabs['SPA'],calc_spa(aq_time))
                                
                                if(line[-1]!='\n'):
                                    line = '{}\n'.format(line)
                                fixedFile.write(line)
                                
                                accData = data.split(',')
                                acc_component = [int(accData[0]),int(accData[1]),int(accData[2])]
                                try:
                                    angle_deg = math.atan(acc_component[1]/acc_component[0])*180/3.142;
                                except:
                                    print("atan error")
                                line = "{},{},{}".format(aq_time,referenceTabs['trough_angle'],wrap_angle(angle_deg))
                                
                                if(line[-1]!='\n'):
                                    line = '{}\n'.format(line)
                                fixedFile.write(line)
                            break
                bar.update()
        bar.close()
        print ("File was created ")
    
'''def file_Organize():
    for i in files:
        fname = i.split("_")
        if fname[0]=="uoc-trough":
            fileDate = fname[-1].split(".")
            folderName = fileDate[0]
            newPath = path+"/"+folderName
            if os.path.exists(newPath):
                print("Folder "+folderName+" is already exist")
                #filesListinFldr = os.listdir(newPath)
                fixFileName = 'FixCSV_'+fname[-1]
                if os.path.exists(newPath+'/'+fixFileName):
                    print(fixFileName+" is available")
                else:
                    csvFix (newPath,i,folderName)
            else :
                os.mkdir(newPath)
                shutil.copy(RepoPath+'/'+i,newPath)
                csvFix (newPath,i,folderName)
  '''                      
if __name__=='__main__':
   # file_Organize()
   input1 = input("Please insert the date of the file.\n")
   if input1 == "0":
       print("Exit")
       pass
   else:
       read_file('../../trough_data/uoc-trough_log_'+input1+'.csv')
"""
AHE code______________________________________

from pvlib import solarposition, tracking
import pandas as pd
import matplotlib.pyplot as plt
import math
import time

#Know referance
ref_180 = -55.55 + 4.18

tag_ref ={
        'C7BF713C':1,
        'acc':2,
        'gyro':3,
        'irrediance_02':4,
        'irrediance_16':5,
        'humi':6,
        'temp':7,
        'spa':8,
        'trough_angle':9,
        'cmd':10,
        }

def calc_spa(ts):
    '''Get SPA angle using time'''

    tz = 'Etc/UCT'
    lat, lon = 6.902103,79.860599
    
    time_stamp = pd.Timestamp(ts, unit='s')
    solpos = solarposition.get_solarposition(time_stamp, lat, lon)

    truetracking_angles = tracking.singleaxis(
        apparent_zenith=solpos['apparent_zenith'],
        apparent_azimuth=solpos['azimuth'],
        axis_tilt=0,
        axis_azimuth=0,
        max_angle=90,
        backtrack=False,  # for true-tracking
        gcr=0.5)  # irrelevant for true-tracking
    
    return truetracking_angles['tracker_theta'][0]

def wrap_angle(raw_angle):
    '''Wriapping angle to v2.1 Ref -55+4.18'''

    if raw_angle > ref_180:
        return raw_angle - (90 + ref_180)
    
    if raw_angle < ref_180:
        return raw_angle +90 - ref_180

def read_file(file_name):
    '''Reading and preprocessing log file'''
    ff =file_name.split('/');
    with open('./ahefix-{}'.format(ff[-1]),'a+') as fp1:
        with open(file_name,"r") as fp:
            for line in fp:
                mqtt_info = line.split(";");
                try:
                    acc_time = float(mqtt_info[0]);
                except:
                    print(line)
                    break
                tags = ['C7BF713C','acc','gyro','irrediance_02','irrediance_16','humi','temp','cmd']
                for tag in tags:
                    if tag in mqtt_info[1]:
                        if 'ACK' in mqtt_info[2]:
                            break

                        line = "{},{},{}".format(acc_time,tag_ref[tag],mqtt_info[-1])
                        
                        if(line[-1]!='\n'):
                            line = '{}\n'.format(line)
                        fp1.write(line)

                        if tag == 'acc':
                            line ="{},{},{}".format(acc_time,tag_ref['spa'],calc_spa(acc_time))
                            
                            if(line[-1]!='\n'):
                                line = '{}\n'.format(line)
                            fp1.write(line)

                            data = mqtt_info[-1].split(',')
                            acc_component = [int(data[0]),int(data[1]),int(data[2])]
                            try:
                                angle_deg = math.atan(acc_component[1]/acc_component[0])*180/3.142;
                            except:
                                print("atan error")
                            line = "{},{},{}".format(acc_time,tag_ref['trough_angle'],wrap_angle(angle_deg))
                            
                            if(line[-1]!='\n'):
                                line = '{}\n'.format(line)
                            fp1.write(line)

                        break;

if __name__=='__main__':

    read_file('../../../trough_data/uoc-trough_log_051020.csv')

"""