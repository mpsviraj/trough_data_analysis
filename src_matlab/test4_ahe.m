%% Manual selection and modification
% select upto 5 points
aa = dir('./final_data/sam*.mat')
%%
load('./final_data/calib_data.mat');

for i = 1: length(aa)
   file_name = strcat('./final_data/',aa(i).name);
  
   
   if (exist('data')==1)
       fprintf(2,'file unloaded\n')
       fprintf("folder :%s\n",aa(i).name);
       
   end
   load(file_name)
   tmp_data = data.data;

   dt = strsplit(aa(i).name,'_');
   dt = strsplit(dt{3},".");

   title(dt{1})
   crrct_data = calib_temp(tmp_data(:,8:end),coeff);
   
   
   [m,y]=max(crrct_data');
   
   
   fil_indx = abs(y-10)<5;
   
   y_data =find(fil_indx);
   imagesc(crrct_data(y_data,:))
   fprintf("Select the best 5 curves\n");
   y =[]
   for ii = 1:5
    [x,yy] = ginput(1);
    y(ii)=yy;
    hold on
    plot(x,yy,'o','LineWidth',3)
   end
   
   data.profile = y_data(fix(y));
   data.lastmodified=datetime();
   [~, name] = system('hostname');
   data.user = name;
   data.calibrated = crrct_data;
%     ii = input('please enter focal height:','s');
%     ii = sscanf(ii,'%f');
%    data.height = ii;
   
   save(strcat('./final_data/',aa(i).name),'data');
   
%    
end
