close
aa = dir('./final_data/sam*.mat');
    colorspec = {[0 0 0]; [0 1 0]; [1 0 0]; [0 0 1]; [0.6 0.1 0.5]; [0.8 0.4 0]; [0.4 0.7 0.2]};
    s_line = [2 3 1 1 1 1 2]; %manuelly selected best data set from previously select 5 points
h=[]
fwhm =[]
    for k=1:length(aa);
    file_name = strcat('./final_data/',aa(k).name);
    load(file_name)
    temp = data.calibrated();
     org_irr = data.data(:,6);
     err = abs(data.data(:,2)-data.data(:,3));
     
     point = data.profile(s_line(k));
    tr_ang = data.data(point,3);
     lbl{k} = strcat(num2str(data.height),' cm, ','@ ',num2str(tr_ang));
   h(k) =  plot(temp(point,:),'o','color',colorspec{k})
    
    hold on  
    ff = fittype('gauss1');
    
    xx = 1:20; 
    xx(16)=[];
    f = fit(xx',temp(point,:)',ff)
    plot(xx,f(xx),'color',colorspec{k})
    fwhm(k,1)= f.c1;
    fwhm(k,2) = data.height;
%     plot(f)
    
end 
title('Temp ptofiles of differnt levels')
xlabel('sensor')
ylabel('Temperature')
legend(h,lbl) 

figure(2)
plot(fwhm(:,2),fwhm(:,1),'o')
 