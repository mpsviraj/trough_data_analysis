clc;
clear;
dataSet = csvread('../Data_Fixed_files/CalibrateTherm/samfix-uoc-trough_log_310820.csv');

MTR_indx =find(dataSet(:,2)==1010); %filtered mercurry thermometer readings
MTR = dataSet(MTR_indx,[1,3]);

TCR_indx =find(dataSet(:,2)==1); %filtered mercurry thermometer readings
TCR = dataSet(TCR_indx,[1,3:end]);

final = zeros(length(MTR_indx),22);

for i = 1:length(MTR_indx)
%   index = i;
  time_stamp = MTR(i,1);
%   print("f\n",time_stamp);
  [m,index]= min(abs(time_stamp - TCR(:,1)));
%   final(i,1)=[time_stamp];
  final(i,:)=[time_stamp,MTR(i,2),TCR(index,2:end-1)];
    
end
sensor_id = 1:20;
sensor_id(16)=[];% 16th sensor has an error.
Xval = final(:,2);
for i = sensor_id
  Yval = final(:,i+2);
  coeif(i,:)= polyfit(Xval,Yval,1);
  plot(final(:,2),final(:,i+2),'o')
  hold on
end
hold on 
x = -10:102;
plot (x,x)
