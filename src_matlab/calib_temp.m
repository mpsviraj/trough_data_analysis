function [temp] = calib_temp(temp_old,calib)
    temp = zeros(size(temp_old));
    for i = 1:20
        temp(:,i)=calib(i,1)*temp_old(:,i)+calib(i,2);
    end
     temp(:,16)=[];
end

