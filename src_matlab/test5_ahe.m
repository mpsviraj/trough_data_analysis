 aa = dir('./final_data/sam*.mat'); 
%  ii = input('please enter file ID:','s');
%  ii = sscanf(ii,'%d');
%  load('./final_data/focal_heights.mat');
% %  file_name = strcat('./final_data/',aa(ii).name);
close all
 for k=1:length(aa);
    file_name = strcat('./final_data/',aa(k).name);
    load(file_name)

     temp = data.calibrated();
     org_irr = data.data(:,6);
     err = abs(data.data(:,2)-data.data(:,3));
     tr_ang = data.data(:,3);
     lbl = {}
     
    %  subplot(3,3,k);
     figure(k)
 for i = data.profile
     %if  (err(i) > 1)
     
        plot(temp(i,:))
        hold on
        lbl{j}=strcat(num2str(org_irr(i)),',err=',num2str(err(i)),' indx=',num2str(i),'tr_angle=',num2str(tr_ang(i)));
        j = j + 1 ;
    % end
 end
 legend(lbl)  
 fn = strsplit(aa(k).name,'_');
 fn = strsplit(fn{3},".");
 ttl = strcat(fn{1},', ',num2str(data.height),' cm')
 title(ttl)
 end